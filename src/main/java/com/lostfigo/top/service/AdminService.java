package com.lostfigo.top.service;

import java.util.List;

import com.lostfigo.top.entity.Admin;

public interface AdminService {
	public List<Admin> findAdmin(String account, String psw);
}
