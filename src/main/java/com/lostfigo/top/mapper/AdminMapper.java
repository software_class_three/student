package com.lostfigo.top.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.lostfigo.top.entity.Admin;

@Mapper
public interface AdminMapper {
	public List<Admin> findAdmin(String account, String psw);
}
